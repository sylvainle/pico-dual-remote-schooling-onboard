# Test script for 2 PPM IN
# PPM n°1 GP4
# PPM n°2 GP5
import time
from machine import UART, Pin, PWM, Timer
import rp2
import _thread
import array as arr

# Pins
pin_ppm1 = 4
pin_ppm2 = 5
sm_ppm1 = 1
sm_ppm2 = 2
pin_pwm_out = [21, 20, 19, 18, 17] # AIL ELE THR RUD CH5 (CH6 reserved to switch from ppm1 to ppm2)
failsafevalues = [1502,1502,0,1502,1502,1502,1502,1502]

# Conf ppm1 in on StateMachine 1
ppm1 = arr.array('I', [])
ppm1arr = arr.array('I', [])
ppm1baton = _thread.allocate_lock()
@rp2.asm_pio()
def ppm1pulsewidth():
    wrap_target()
    
    # Wait RISING
    #wait(1, pin, 0)                       # 0 (QD pin à 1)
    label("12")
    set(x, 0)                             # 1 (écrire zéro dans registre x)
    
    # WHILE NOT FALLING x--
    label("13")
    jmp(x_dec, "14")                       # 3 (x--)
    label("14")
    jmp(pin, "13")                         # 4 (TQ pin est à 1 GOTO 3)
    
    # WHILE NOT RISING x--
    label("15")
    jmp(pin, "16")
    jmp(x_dec, "15")
    label("16")

    # WRITE uS + IQR
    mov(isr, x)                           # 5 (copy registre x dans ISR)
    push(isr, block)                      # 6 (copy ISR dans RX)
    irq(1)                         # 7 (trigger state machine IRQ)
    jmp("12")    
    wrap()

def ppm1handler(sm1):
    global ppm1, ppm1arr

    # x-reg counts down
    value = 0x100000000 - sm1.get()
    ppm1baton.acquire()
    try:
        if value > 5000 and len(ppm1arr) == 8:         # EOF (End Of Frame)
            ppm1 = ppm1arr
            ppm1arr = []
        elif value > 800 and value < 2200:             # A channel value
            ppm1arr.append(value)
        else:                                          # An error
            ppm1arr = []
    except:
        pass
    ppm1baton.release()

ppm1pin = Pin(pin_ppm1, Pin.IN, Pin.PULL_UP)
ppm1sm = rp2.StateMachine(sm_ppm1, ppm1pulsewidth, freq=2_000_000, in_base=ppm1pin, jmp_pin=ppm1pin)
ppm1sm.irq(ppm1handler)

# Conf ppm2 in on StateMachine 2
ppm2 = arr.array('I', [])
ppm2arr = arr.array('I', [])
ppm2baton = _thread.allocate_lock()
@rp2.asm_pio()
def ppm2pulsewidth():
    wrap_target()
    
    # Wait RISING
    #wait(1, pin, 0)                       # 0 (QD pin à 1)
    label("22")
    set(x, 0)                             # 1 (écrire zéro dans registre x)
    
    # WHILE NOT FALLING x--
    label("23")
    jmp(x_dec, "24")                       # 3 (x--)
    label("24")
    jmp(pin, "23")                         # 4 (TQ pin est à 1 GOTO 3)
    
    # WHILE NOT RISING x--
    label("25")
    jmp(pin, "26")
    jmp(x_dec, "25")
    label("26")

    # WRITE uS + IQR
    mov(isr, x)                           # 5 (copy registre x dans ISR)
    push(isr, block)                      # 6 (copy ISR dans RX)
    irq(2)                                # 7 (trigger state machine IRQ)
    jmp("22")    
    wrap()

def ppm2handler(sm2):
    global ppm2, ppm2arr

    value = 0x100000000 - sm2.get()
    ppm2baton.acquire()
    try:
        if value > 5000 and len(ppm2arr) == 8:        # EOF (End Of Frame)
            ppm2 = ppm2arr
            ppm2arr = []
        elif value > 800 and value < 2200:            # A channel value
            ppm2arr.append(value)
        else:                                         # An error
            ppm2arr = []
    except:   
        pass
    ppm2baton.release()

ppm2pin = Pin(pin_ppm2, Pin.IN, Pin.PULL_UP)
ppm2sm = rp2.StateMachine(sm_ppm2, ppm2pulsewidth, freq=2_000_000, in_base=ppm2pin, jmp_pin=ppm2pin)
ppm2sm.irq(ppm2handler)

# Conf pwm out
mid_pwm = 1502  # mid 1502 (Pro tronik PTR6A v2 + XR8 ppm)
width_pwm = 600 # min 902 max 2102
center_u16_duty = 4915
width_u16_duty = 1950
servos = []
for i in pin_pwm_out:
    servos.append(PWM(Pin(i)))
    servos[-1].freq(50)
def calcDuty(pwmvalue):
    global u16_duty

    try:
        u16_duty = (pwmvalue - mid_pwm + width_pwm) * (center_u16_duty + width_u16_duty) / (mid_pwm + width_pwm) + (center_u16_duty - width_u16_duty)
    except:
        u16_duty = 0
    return int(u16_duty)
timRcPwm = Timer()
def timRcPwm_Handler(timer):
    global rcPwm
    global pwm1
    global failsafevalues


    try:
        # Read PPM 1 array
        ppm1baton.acquire()
        try:
            ppm1values = ppm1
        except:
            ppm2values = []
        ppm1baton.release()

        # Read PPM 2 array
        ppm2baton.acquire()
        try:
            ppm2values = ppm2
        except:
            ppm2values = []
        ppm2baton.release()

        # Write PWM values
        if len(ppm1values) != 8 and len(ppm2values) != 8:    # ppm1 KO ppm2 KO ==> failsafe
            ppmvalues = failsafevalues
        if len(ppm1values) == 8 and len(ppm2values) != 8:    # ppm1 OK ppm2 KO ==> ppm1
            ppmvalues = ppm1values
        elif len(ppm1values) != 8 and len(ppm2values) == 8 : # ppm1 KO ppm2 OK ==> ppm2
            ppmvalues = ppm2values
        elif ppm1values[5] > 900 and ppm1values[5] < 1100:  # ppm1 selected
            ppmvalues = ppm1values
        elif ppm1values[5] > 1100:                          # ppm2 selected
            ppmvalues = ppm2values
        else:
            ppmvalues = failsafevalues
            
        #print(tuple(ppmvalues))
        servos[0].duty_u16(int(calcDuty(ppmvalues[0])))
        servos[1].duty_u16(int(calcDuty(ppmvalues[1])))
        servos[2].duty_u16(int(calcDuty(ppmvalues[2])))
        servos[3].duty_u16(int(calcDuty(ppmvalues[3])))
        servos[4].duty_u16(int(calcDuty(ppmvalues[4])))
    except:
        pass   
    
    # debug
    #print('{} {} {} {} {}'.format(tmp, tmp - mid_pwm + width_pwm, (tmp - mid_pwm + width_pwm) * (center_u16_duty + width_u16_duty), (tmp - mid_pwm + width_pwm) * (center_u16_duty + width_u16_duty) / (mid_pwm + width_pwm), (tmp - mid_pwm + width_pwm) * (center_u16_duty + width_u16_duty) / (mid_pwm + width_pwm) + (center_u16_duty - width_u16_duty)))

# Start state machines and timers
ppm1sm.active(1)
ppm2sm.active(1)
timRcPwm.init(freq=50, mode=Timer.PERIODIC, callback=timRcPwm_Handler)

# Loooop
led = Pin(25, Pin.OUT)
r12 = 0
r22 = 0
r13 = []
r23 = []
while True:
    # activity indication
    led.toggle()
       
    # read ppm1
    #try:
    #    ppm1baton.acquire()
    #    try:
    #        r13 = array(ppm1)
    #    except:
    #        pass
    #    ppm1baton.release()
    #except:
    #    print('Pas de ppm1')

    # read ppm2
    #try:
    #    ppm2baton.acquire()
    #    try:
    #        r23 = array(ppm2)
    #    except:
    #        pass
    #    ppm2baton.release()
    #except:
    #    print('Pas de ppm2')

    # print
    #print('PPM 1 {} PPM 2 {}'.format(tuple(r13), tuple(r23)))
    
    time.sleep(1)
